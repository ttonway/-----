package com.crandtec.tmms.android.app;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created with IntelliJ IDEA.
 * User: ttonway
 * Date: 14/11/3
 * Time: 上午11:30
 */
public class BaseActivity extends ActionBarActivity  {

    TextView _mActionBarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.actionbar);
            _mActionBarTitle = (TextView) actionBar.getCustomView().findViewById(R.id.actionbar_title);
            _mActionBarTitle.setText(getTitle());
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        _mActionBarTitle = null;
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);

        if (_mActionBarTitle != null) {
            _mActionBarTitle.setText(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
