package com.crandtec.tmms.android.app;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.Arrays;

/**
 * Created by ttonway on 16/3/5.
 */
public class MyWebViewClient extends WebViewClient {
    private static final String TAG = "MyWebViewClient";

    boolean mLoadFinished;

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        //Log.d(TAG, "onPageStarted " + url);
        mLoadFinished = false;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        //Log.d(TAG, "onPageFinished " + url);
        mLoadFinished = true;

//        String js = "var newscript = document.createElement(\"script\");";
//        js += "newscript.src=\"http://192.168.2.142:8880/target/target-script-min.js#anonymous\";";
//        js += "document.body.appendChild(newscript);";
//        view.loadUrl("javascript:" + js);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        Log.d(TAG, "shouldOverrideUrlLoading " + url);
        if (!mLoadFinished) {
            return false;
        }

        Uri uri = Uri.parse(url);
        if (Arrays.asList(MainActivity.PAGE_URL).contains(url)) {
            view.loadUrl(url);
        } else {
            Intent intent = new Intent(view.getContext(), PageActivity.class);
            intent.setAction(Intent.ACTION_VIEW);
            intent.setData(uri);
            view.getContext().startActivity(intent);
        }
        return true;
    }

    @Override
    public void onReceivedError(WebView view, int errorCode,
                                String description, String failingUrl) {
        Log.e(TAG, "onReceivedError " + failingUrl + " " + errorCode + ":" + description);
        if (errorCode == ERROR_HOST_LOOKUP) {
            description = view.getContext().getString(R.string.network_unavailable);
        }
        String errorHtml = "<html><body>" +
                "<div style=\"width:200px; height:200px; position:absolute; left:50%; top:50%; margin-left:-100px; margin-top:-100px; text-align:center; line-height:200px;\">" + description + "</div>" +
                "</body></html>";
        Log.d(TAG, "load error html: " + errorHtml);
        view.loadData(errorHtml, "text/html; charset=UTF-8", null);
    }
}
