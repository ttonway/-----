package com.crandtec.tmms.android.app;

import android.annotation.TargetApi;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

/**
 * Created by ttonway on 16/3/5.
 */
public class PageActivity extends BaseActivity {
    private static final String TAG = "PageActivity";

    ProgressBar mProgressBar;
    FrameLayout mFullScreenContainer;
    WebView mWebView;
    boolean mIsPaused;

    private View mCustomView;
    private WebChromeClient.CustomViewCallback mCustomViewCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
        mFullScreenContainer = (FrameLayout) findViewById(R.id.fullView);
        mWebView = (WebView) findViewById(R.id.webView);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mWebView.setWebViewClient(new MyWebViewClient());
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);

                PageActivity.this.setTitle(title);
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (mProgressBar != null) {
                    if (newProgress < 100) {
                        mProgressBar.setVisibility(View.VISIBLE);
                        mProgressBar.setProgress(newProgress);
                    } else {
                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onShowCustomView(View view, CustomViewCallback callback) {
                super.onShowCustomView(view, callback);
                Log.i(TAG, "onShowCustomView " + view);

                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                mWebView.setVisibility(View.INVISIBLE);

                if (mCustomView != null) {
                    callback.onCustomViewHidden();
                    return;
                }

                mFullScreenContainer.addView(view);
                mCustomView = view;
                mCustomViewCallback = callback;
                mFullScreenContainer.setVisibility(View.VISIBLE);
                getSupportActionBar().hide();
            }

            @Override
            public void onHideCustomView() {
                super.onHideCustomView();
                Log.i(TAG, "onHideCustomView");

                if (mCustomView == null)
                    return;
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                mCustomView.setVisibility(View.GONE);
                mFullScreenContainer.removeView(mCustomView);
                mCustomView = null;
                mFullScreenContainer.setVisibility(View.GONE);
                mCustomViewCallback.onCustomViewHidden();
                mWebView.setVisibility(View.VISIBLE);
                getSupportActionBar().show();
            }
        });

        Uri data = getIntent().getData();
        mWebView.loadUrl(data.toString());
    }

    @TargetApi(11)
    @Override
    protected void onResume() {
        super.onResume();
        if (mWebView != null) {
            mWebView.reload();
            if (Utils.hasHoneycomb()) {
                mWebView.onResume();
            }
            mWebView.resumeTimers();
        }
        mIsPaused = false;
    }

    @TargetApi(11)
    @Override
    protected void onPause() {
        super.onPause();
        mIsPaused = true;
        if(mWebView != null) {
            mWebView.stopLoading();
            if (Utils.hasHoneycomb()) {
                mWebView.onPause(); //pauses background threads, stops playing sound
            }
            mWebView.pauseTimers(); //pauses the WebViewCore
        }
    }
}
