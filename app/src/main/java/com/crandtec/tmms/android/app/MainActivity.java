package com.crandtec.tmms.android.app;

import android.annotation.TargetApi;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;

/**
 * Created by ttonway on 16/3/4.
 */
public class MainActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {

    public static final String[] PAGE_URL = new String[] {
            "http://tmms.crandtec.com/m/index.php",//0
            "http://tmms.crandtec.com/m/list.php?tid=52&tn=0",
            "http://tmms.crandtec.com/m/list.php?tid=52&tn=4",
            "http://tmms.crandtec.com/m/list.php?tid=50",//3
            "http://tmms.crandtec.com/m/list.php?tid=52&tn=0",
            "http://tmms.crandtec.com/m/list.php?tid=52&tn=4",
            "http://tmms.crandtec.com/m/list.php?tid=13",//6
            "http://tmms.crandtec.com/m/list.php?tid=52&tn=0",
            "http://tmms.crandtec.com/m/list.php?tid=52&tn=4",
            "http://tmms.crandtec.com/m/list.php?tid=51",//9
            "http://tmms.crandtec.com/m/list.php?tid=52&tn=0",
            "http://tmms.crandtec.com/m/list.php?tid=52&tn=4",
            "http://tmms.crandtec.com/m/list.php?tid=20",//12
            "http://tmms.crandtec.com/m/list.php?tid=52&tn=0",
            "http://tmms.crandtec.com/m/list.php?tid=52&tn=4"
    };

    RadioGroup mRadioGroup;
    ProgressBar mProgressBar;
    WebView mWebView;
    boolean mIsPaused;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRadioGroup = (RadioGroup) findViewById(R.id.tab_group);
        mRadioGroup.setOnCheckedChangeListener(this);
//        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
        mWebView = (WebView) findViewById(R.id.webView);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mWebView.setWebViewClient(new MyWebViewClient());
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (mProgressBar != null) {
                    if (newProgress < 100) {
                        mProgressBar.setVisibility(View.VISIBLE);
                        mProgressBar.setProgress(newProgress);
                    } else {
                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }
        });
        mRadioGroup.check(R.id.tab1);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.tab1:
                mWebView.loadUrl(PAGE_URL[0]);
                break;
            case R.id.tab2:
                mWebView.loadUrl(PAGE_URL[3]);
                break;
            case R.id.tab3:
                mWebView.loadUrl(PAGE_URL[6]);
                break;
            case R.id.tab4:
                mWebView.loadUrl(PAGE_URL[9]);
                break;
            case R.id.tab5:
                mWebView.loadUrl(PAGE_URL[12]);
                break;
        }
    }

    @TargetApi(11)
    @Override
    protected void onResume() {
        super.onResume();
        if (mWebView != null) {
            mWebView.reload();
            if (Utils.hasHoneycomb()) {
                mWebView.onResume();
            }
            mWebView.resumeTimers();
        }
        mIsPaused = false;
    }

    @TargetApi(11)
    @Override
    protected void onPause() {
        super.onPause();
        mIsPaused = true;
        if (mWebView != null) {
            mWebView.stopLoading();
            if (Utils.hasHoneycomb()) {
                mWebView.onPause();
            }
            mWebView.pauseTimers();
        }
    }
}
